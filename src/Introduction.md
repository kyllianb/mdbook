# Graphbuilder

---

Collection de Ressources permettant de produire de diagrammes et synoptiques avec FME à partir d'une structure de graphe : succession de noeuds et d'arcs.  
Ces outils n'exploitent pas la géométrie des données, mais leurs relation topologiques ou d'attributs.  
Le format de sortie type est PDF, XLSX ou DWG  

---
